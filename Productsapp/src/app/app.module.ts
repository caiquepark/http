import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


 

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './Material.Module';
import { DepartmentComponent } from './views/department/department.component';
import { ProductComponent } from './views/product/product.component';



@NgModule({
  declarations: [
    AppComponent,
    DepartmentComponent,
    ProductComponent
  
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule,
    


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
