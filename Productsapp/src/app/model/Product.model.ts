
import { DepartmentModel } from "./Department.model";

export interface ProductModel {
    name: string;
    departments: any[];
    _id ?: string;
    stock: number;
    price: number;
    
    
}