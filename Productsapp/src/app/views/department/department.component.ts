
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { DepartmentModel } from 'src/app/model/Department.model';
import { DepartmentService } from 'src/app/service/department.service';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.sass']
})
export class DepartmentComponent implements OnInit {
  
  depName: string = '';
  departments: DepartmentModel[] = [];
  private unSubscribe$: Subject<any> = new Subject();
  idEdit: DepartmentModel = {
    name: '',
    _id: ''
  };
  


  constructor( private deparService: DepartmentService,
    private snackBar: MatSnackBar) {}
      
     

  ngOnInit(): void {

    
   this.deparService.get()
   .subscribe((departs) => this.departments = departs )

    this.deparService.get()
    .pipe(takeUntil(this.unSubscribe$))
    .subscribe((deps) => this.departments = deps);

   

  }

  save() {
    if(this.idEdit.name != '') {

      console.log(this.deparService.update({name: this.depName, _id: this.idEdit._id}));

      this.deparService.update({name: this.depName, _id: this.idEdit._id})
      .subscribe(
        (dep) => {
          console.log(dep);
          this.notFy("Departamento editado salvo");
          
        },
        (err) => {
          this.notFy('Error na edição');
          console.log(err);
        }
      );
    }else {
      this.deparService.add({name: this.depName})
      .subscribe(
        (dep) => {
          if(dep)
          this.notFy('Novo departamento salvo')
           
        },
        (err) => {
          if(err)
          console.log(err);
        }
      )
      
    }
    this.clearFields();
  }

  clearFields(){ 
   this.depName = '';
   this.idEdit = {
     name: '',
     _id: '', 
   }   
  }

  cancel() {
    this.clearFields();
  }

  edit(dp: DepartmentModel) {
   
    this.depName = dp.name;
    this.idEdit = dp;
  

  }

  delet(dp: DepartmentModel) {
    this.deparService.delete(dp)
    .subscribe(
      () => this.notFy('Departmento removido!'),
      (err) => console.error(err)
    )
  }

  notFy(msg: String) {
    this.snackBar.open(msg.toString(), "ok", {duration: 3000});
  }

  ngOnDestroy() {
    this.unSubscribe$.next();
  }

}
