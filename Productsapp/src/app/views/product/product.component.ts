import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DepartmentModel } from 'src/app/model/Department.model';
import { ProductModel } from 'src/app/model/Product.model';
import { DepartmentService } from 'src/app/service/Department.service';
import { ProductService } from 'src/app/service/Product.service';




@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.sass']
})
export class ProductComponent implements OnInit {


  productForm: FormGroup = this.fb.group({
    _id: [null],
    name: ['',[Validators.required]],
    stock: [0,[Validators.required, Validators.min(0)]],
    price: [0,[Validators.required, Validators.min(0)]],
    departments: [[], [Validators.required]] 

  });



  
  @ViewChild('form')
  form!: NgForm;

  products: ProductModel[] = [];
  departments: DepartmentModel [] = [];

  private unSubscribe$: Subject<any> = new Subject<any>();

  constructor(
    private productService: ProductService,
    private fb: FormBuilder,
    private departmentService: DepartmentService,
    private snackBar: MatSnackBar,
    
    ) { }

  ngOnInit(): void {
    this.productService.get()
      .pipe(takeUntil(this.unSubscribe$))
      .subscribe((prods)=> this.products = prods);
    
    this.departmentService.get()
      .pipe(takeUntil(this.unSubscribe$))
      .subscribe((deps)=> this.departments = deps);
      console.log('o que eu estou vendo',this.products);
  }
 
  ngOnDestroy() {
    this.unSubscribe$.next();
  }

  

  save(){
    let data = this.productForm.value;
    if(data._id != null){
       this.productService.update(data)
       .subscribe(
         (p)=> this.notFy('Produto ediato com sucesso!'),
         
       );
    } else {
      this.productService.add(data)
      .subscribe(
        (p) => this.notFy("Produto inserido com sucesso!")
      );
    }
    this.resetForms(); 
  }

  delete(p: ProductModel){
    this.productService.delete(p)
    .subscribe(
      ()=> this.notFy("Produto foi deletado"),
      (err)=> this.notFy(err.error.msg)
    )
  }
   
  edit(p: ProductModel) {
    this.productForm.setValue(p)
  }
  
  notFy(msg: String) {
    this.snackBar.open(msg.toString(), "Ok", {duration: 3000});
  }

  
  resetForms() { 
    this.form.resetForm();
  }






}
