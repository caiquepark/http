import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';
import { DepartmentModel } from '../model/Department.model';
import { ProductModel } from '../model/Product.model';
import { DepartmentService } from './Department.service';



@Injectable({
  providedIn: 'root'
})
export class ProductService {
  
  readonly url = 'http://localhost:3000/products';
  private productSubject$: BehaviorSubject<ProductModel[]> = new  BehaviorSubject<ProductModel[]>([]) ;
  private loaded: boolean = false;


  constructor
  (private http: HttpClient,
    private departmentService: DepartmentService
  ) {
   
   }


 get(): Observable<ProductModel[]>{
    
    if(!this.loaded) {
       combineLatest(
         [this.http.get<ProductModel[]>(this.url),
          this.departmentService.get()])
          .pipe(
            tap(([products, departments]) => console.log(products, departments)),
            filter(([products, departments])=> products != null && departments != null), 
            map(([products, departments]) => {
              for(let p of products) {
                let ids = (p.departments as string[]);
                (p.departments as any)  = ids.map((id)=>departments.find(dep=>dep._id==id));
                 
              }
              return products;          
             }),
            
            
          )  
          .subscribe(this.productSubject$);
           this.loaded = true;

    }
    return this.productSubject$.asObservable();

  }

  add(prod: ProductModel): Observable<ProductModel> {
    let departments = (prod.departments as DepartmentModel[]).map(d=>d._id);
    return this.http.post<ProductModel>(this.url, {...prod, departments})
    .pipe(
      tap((p) => {
        this.productSubject$.getValue()
        .push({...prod, _id: p._id})
      })  

    )
  }

  delete(prod: ProductModel): Observable<any> {
    return this.http.delete(`${this.url}/${prod._id}`)
    .pipe(
      tap(()=> {
      let products = this.productSubject$.getValue();
       let i =  products.findIndex(p=> p._id === prod._id);
       if(i>=0)
       products.splice(i, 1);
      })
    )
  }

  update(prod: ProductModel): Observable<ProductModel> {
    let departments = (prod.departments as DepartmentModel[]).map(d=>d._id);
    return this.http.patch<ProductModel>(`${this.url}/${prod._id}`, {...prod, departments})
    .pipe(
      tap( () => {
        let products = this.productSubject$.getValue();
        let i =  products.findIndex(d=> d._id === prod._id);
          if(i>=0)
          products[i]= prod;
      })
    ) 
  }






}





