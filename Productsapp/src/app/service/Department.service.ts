import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, pipe} from 'rxjs';
import { tap } from 'rxjs/operators'
import { DepartmentModel } from '../model/Department.model';


@Injectable({
  providedIn: 'root'
})
export class DepartmentService {


  readonly url = 'http://localhost:3000/departments';

  private departmentSubject$: BehaviorSubject<DepartmentModel[]> = new  BehaviorSubject<DepartmentModel[]>([]) ;
  private loaded: boolean = false;

  constructor(private http: HttpClient ) { }

  get(): Observable<DepartmentModel[]> {
    if(!this.loaded){
      this.http.get<DepartmentModel[]>(this.url)
      .pipe(tap(deps => console.log(deps)))
      .subscribe(this.departmentSubject$);
      this.loaded = true;
      
    }
    return this.departmentSubject$.asObservable();

  }

  add(depar: DepartmentModel): Observable<DepartmentModel> {
    return this.http.post<DepartmentModel>(this.url, depar)
    .pipe(tap((dep: DepartmentModel) => this.departmentSubject$.getValue().push(dep)))


  }
  
  delete(dep: DepartmentModel): Observable<any> {
    return this.http.delete(`${this.url}/${dep._id}`)
    .pipe(tap(() => {
       let departments = this.departmentSubject$.getValue();
       let i =  departments.findIndex(d=> d._id === dep._id);
       if(i>=0)
       departments.splice(i, 1);
    }));
  }

  update(dep: DepartmentModel): Observable<DepartmentModel> {
    console.log(this.http.patch<DepartmentModel>(`${this.url}/${dep._id}`, dep));
    return this.http.patch<DepartmentModel>(`${this.url}/${dep._id}`, dep)
    .pipe(tap((d) => {
        let departments = this.departmentSubject$.getValue();
        let i =  departments.findIndex(d=> d._id === dep._id);
        if(i>=0)
        departments[i].name = d.name;
        
      }));
  }

}
